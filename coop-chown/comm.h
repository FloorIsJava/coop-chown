#ifndef COMM_H
#define COMM_H

#include <stdint.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

typedef struct comm_address
{
    uid_t offerer;
    uid_t requester;
    ino_t target_file;
} comm_address;

typedef struct comm_server
{
    int32_t      socket_fd;
    int32_t      requester_fd;
    comm_address address;
} comm_server;

void comm_create_server( uid_t requester, ino_t target, comm_server* server );
void comm_wait_requester( comm_server* server );
void comm_delete_server( comm_server* server );

void comm_request_adopt( uid_t offerer, ino_t target );

#endif // COMM_H
