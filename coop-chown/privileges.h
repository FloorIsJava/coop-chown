#ifndef PRIVILEGES_H
#define PRIVILEGES_H

#include <stdbool.h>

// Permanently drop privileges
void priv_drop_perm( void );

// Temporarily drop privileges
void priv_drop( void );

// Recover temporarily dropped privileges
void priv_recover( void );

// Sanitizes privileges, that is, sets the EGID to 0, if EUID is 0.
void priv_sanitize( void );

// Returns true if superuser privileges are effective
bool priv_check( void );

#endif // PRIVILEGES_H
