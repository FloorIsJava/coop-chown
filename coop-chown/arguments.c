#include <errno.h>
#include <stdlib.h>

#include "./arguments.h"
#include "./logging.h"

chown_args g_args;

static void show_help( void )
{
    printf( "coop-chown -h                    - show this help\n" );
    printf( "coop-chown -u <uid> [-gv] <path> - offer ownership to <uid>\n" );
    printf( "coop-chown [-gv]          <path> - accept ownership\n" );
    exit( EXIT_SUCCESS );
}

void args_init( void )
{
    g_args.has_offer = false;
    g_args.set_group = false;
    g_args.offer_to  = 0;
    g_args.verbose   = 0;
    g_args.path      = NULL;
}

void args_parse( int32_t argc, char** argv )
{
    int32_t option = 0;
    while( option != -1 )
    {
        // -u <uid>  offer to <uid>
        // -g        set group to primary group of recipient (requires -u)
        // -h        print help and exit
        // -v[v[v]   verbose
        option = getopt( argc, argv, "+u:ghv" );
        switch( option )
        {
        case 'g': // set group to primary group of recipient (requires -u)
            g_args.set_group = true;
            break;

        case 'u': // offer to <uid>
            g_args.has_offer = true;

            errno = 0;
            char* endptr = NULL;
            g_args.offer_to = strtoul( optarg, &endptr, 10 );
            if( endptr == NULL || endptr[ 0 ] != 0 || optarg[ 0 ] == 0 || errno != 0 )
            {
                printf_die( "Invalid value for user id\n" );
            }
            break;

        case 'v': // verbose
            if( g_args.verbose < 2 )
            {
                ++g_args.verbose;
            }
            break;

        case -1:
            break;

        case '?':
        case 'h': // print help and exit
            show_help();
            break;

        default:
            printf_bug( "getopt return unhandled (%d)\n", option );
        }
    }

    // Argument sanity checks
    if( optind >= argc )
    {
        show_help();
        // noreturn
    }
    g_args.path = argv[ optind ];
}
