#include <stdint.h>

#include <fcntl.h>
#include <pwd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "./arguments.h"
#include "./comm.h"
#include "./logging.h"
#include "./offer.h"
#include "./privileges.h"

typedef struct offer_data
{
    int32_t      inode_fd;
    struct stat  inode_info;
} offer_data;

static void check_file_type_allowed( mode_t mode )
{
    printf_vv( g_args.verbose, "Checking file type %u\n", mode );
    if( !S_ISREG( mode ) )
    {
        // :TODO: allow more file types? would need a different open() handling...
        printf_die( "Not a regular file\n" );
    }
}

static void close_path( offer_data* data )
{
    printf_vv( g_args.verbose, "Attempting to close() file descriptor %d\n", data->inode_fd );
    if( close( data->inode_fd ) == -1 )
    {
        printf_die( "close() failed on file descriptor %d\n", data->inode_fd );
    }
}

static void open_path( offer_data* data )
{
    printf_vv( g_args.verbose, "Performing lstat() on %s\n", g_args.path );
    if( lstat( g_args.path, &data->inode_info ) == -1 )
    {
        printf_die( "Can not lstat() path on %s\n", g_args.path );
    }

    check_file_type_allowed( data->inode_info.st_mode );

    printf_vv( g_args.verbose, "Opening file %s\n", g_args.path );
    data->inode_fd = open( g_args.path, O_RDONLY | O_NOFOLLOW );
    if( data->inode_fd == -1 )
    {
        printf_die( "open() of %s failed\n", g_args.path );
    }

    // Replace inode_info by fstat() on the actual open file, to mitigate races
    printf_vv( g_args.verbose, "Performing fstat() on file descriptor %d\n", data->inode_fd );
    if( fstat( data->inode_fd, &data->inode_info ) == -1 )
    {
        printf_die( "Can not fstat() file descriptor %d\n", data->inode_fd );
    }

    // Check again, might have changed due to a race
    check_file_type_allowed( data->inode_info.st_mode );

    printf_vv( g_args.verbose, "Checking file owner of inode %lu\n", data->inode_info.st_ino );
    if( data->inode_info.st_uid != getuid() )
    {
        printf_die( "Permission denied\n" );
    }
    // Only privileged users can change owner now, so it is *possible* that someone interferes...
    // However, that someone is privileged, so we ignore that.
}

static void chown_file( const offer_data* data )
{
    gid_t to_group = -1;
    if( g_args.set_group )
    {
        struct passwd* pw = getpwuid( g_args.offer_to );
        if( pw == NULL )
        {
            printf_die( "Can not obtain passwd struct for user %u\n", g_args.offer_to );
        }
        to_group = pw->pw_gid;

        printf_v( g_args.verbose
                , "%s: owner %u -> %u, group %u -> %u\n"
                , g_args.path
                , data->inode_info.st_uid
                , g_args.offer_to
                , data->inode_info.st_gid
                , to_group );
    }
    else
    {
        printf_v( g_args.verbose
                , "%s: owner %u -> %u\n"
                , g_args.path
                , data->inode_info.st_uid
                , g_args.offer_to );
    }

    priv_recover();
    if( fchown( data->inode_fd, g_args.offer_to, to_group ) == -1 )
    {
        printf_die( "Failed to fchown() file descriptor %d\n", data->inode_fd );
    }
    priv_drop_perm();
}

void offer_perform( void )
{
    // 1. Open the path to obtain a consistent fd, to avoid file swapping trickery
    // 2. Ensure the owner is the RUID
    printf_vv( g_args.verbose
             , "Opening %s to obtain a consistent file descriptor\n"
             , g_args.path );
    offer_data data;
    open_path( &data );

    // 3. Set up a unix domain socket for accept requests
    comm_server server;
    comm_create_server( g_args.offer_to, data.inode_info.st_ino, &server );

    // 4. Wait for an adopt request for this inode
    // 5. Check the peer UID, ensure it is equal to the requested UID
    comm_wait_requester( &server );

    // 6. Change ownership of the file to peer UID
    chown_file( &data );

    comm_delete_server( &server );
    close_path( &data );
}
