#include <sys/stat.h>
#include <sys/types.h>

#include "./adopt.h"
#include "./arguments.h"
#include "./comm.h"
#include "./logging.h"

void adopt_perform( void )
{
    printf_vv( g_args.verbose, "Performing lstat() on %s\n", g_args.path );
    struct stat info;
    if( lstat( g_args.path, &info ) == -1 )
    {
        printf_die( "Can not lstat() %s\n", g_args.path );
    }

    printf_vv( g_args.verbose, "Requesting adopt of %lu from %u\n", info.st_ino, info.st_uid );
    comm_request_adopt( info.st_uid, info.st_ino );
    printf_v( g_args.verbose, "Adopt request sent.\n" );
}
