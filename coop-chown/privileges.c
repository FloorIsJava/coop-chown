#include <unistd.h>

#include "./arguments.h"
#include "./logging.h"
#include "./privileges.h"

void priv_drop_perm( void )
{
    gid_t real_gid = getgid();
    printf_vv( g_args.verbose, "Dropping EGID permanently from %u to %u\n", getegid(), real_gid );
    if( setresgid( real_gid, real_gid, real_gid ) == -1 )
    {
        printf_die( "Failed to drop EGID, SGID to RGID\n" );
    }

    uid_t real_uid = getuid();
    printf_vv( g_args.verbose, "Dropping EUID permanently from %u to %u\n", geteuid(), real_uid );
    if( setresuid( real_uid, real_uid, real_uid ) == -1 )
    {
        printf_die( "Failed to drop EUID, SUID to RUID\n" );
    }
}

void priv_drop( void )
{
    gid_t real_gid = getgid();
    printf_vv( g_args.verbose, "Dropping EGID temporarily from %u to %u\n", getegid(), real_gid );
    if( setegid( real_gid ) == -1 )
    {
        printf_die( "Failed to drop EGID to RGID\n" );
    }

    uid_t real_uid = getuid();
    printf_vv( g_args.verbose, "Dropping EUID permanently from %u to %u\n", geteuid(), real_uid );
    if( seteuid( real_uid ) == -1 )
    {
        printf_die( "Failed to drop EUID to RUID\n" );
    }
}

void priv_recover( void )
{
    gid_t rgid, egid, sgid;
    if( getresgid( &rgid, &egid, &sgid ) == -1 )
    {
        printf_die( "Failed to obtain SGID\n" );
    }
    printf_vv( g_args.verbose, "Recovering EGID from SGID, from %u to %u\n", egid, sgid );
    if( setegid( sgid ) == -1 )
    {
        printf_die( "Failed to recover EGID from SGID\n" );
    }

    uid_t ruid, euid, suid;
    if( getresuid( &ruid, &euid, &suid ) == -1 )
    {
        printf_die( "Failed to obtain SUID\n" );
    }
    printf_vv( g_args.verbose, "Recovering EUID from SUID, from %u to %u\n", euid, suid );
    if( seteuid( suid ) == -1 )
    {
        printf_die( "Failed to recover EUID from SUID\n" );
    }
}

void priv_sanitize( void )
{
    if( priv_check() )
    {
        printf_vv( g_args.verbose, "Sanitizing EGID, SGID\n" );
        if( setresgid( getgid(), 0, 0 ) == -1 )
        {
            printf_die( "Failed to sanitize EGID, SGID\n" );
        }
    }
}

bool priv_check( void )
{
    return geteuid() == 0;
}
