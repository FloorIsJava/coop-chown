#include <signal.h>
#include <stddef.h>
#include <stdint.h>

#include "./adopt.h"
#include "./arguments.h"
#include "./logging.h"
#include "./offer.h"
#include "./privileges.h"

int main( int32_t argc, char** argv )
{
    args_init();
    signal( SIGPIPE, SIG_IGN );

    // exec() and friends
    if( argc < 1 )
    {
        printf_die( "Missing argv[0].\n" );
    }

    // Enforce being superuser
    if( !priv_check() )
    {
        printf_die( "Effective user is not root. Is the binary suid-root?\n" );
    }
    priv_sanitize();
    priv_drop();

    args_parse( argc, argv );

    if( g_args.has_offer )
    {
        printf_v( g_args.verbose, "Offering %s for adoption...\n", g_args.path );
        offer_perform();
    }
    else
    {
        printf_v( g_args.verbose, "Trying to adopt %s...\n", g_args.path );
        priv_drop_perm();
        adopt_perform();
    }

    return EXIT_SUCCESS;
}
