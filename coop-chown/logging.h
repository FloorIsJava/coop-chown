#ifndef LOGGING_H
#define LOGGING_H

#include <stdio.h>
#include <stdlib.h>

#define printf_bug( ... ) fprintf( stderr, "[!BUG!] " __VA_ARGS__ ); abort()
#define printf_die( ... ) fprintf( stderr, "[ERROR] " __VA_ARGS__ ); exit( EXIT_FAILURE )
#define printf_v( X, ... ) if( X > 0 ) printf( "[ INFO] " __VA_ARGS__ )
#define printf_vv( X, ... ) if( X > 1 ) printf( "[DEBUG] " __VA_ARGS__ )

#endif // LOGGING_H
