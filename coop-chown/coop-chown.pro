TEMPLATE = app
CONFIG += console
CONFIG += objects_parallel_to_source
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CFLAGS_APP =
QMAKE_CFLAGS_WARN_ON =
QMAKE_CFLAGS_WARN_OFF =
QMAKE_LFLAGS =

QMAKE_CFLAGS = -std=c11 -Wall -Wextra -Werror -pedantic -fPIE
QMAKE_CFLAGS_DEBUG = -O0 -g3
QMAKE_CFLAGS_RELEASE = -O3 -flto
QMAKE_LFLAGS_DEBUG =
QMAKE_LFLAGS_RELEASE = -flto -Wl,--as-needed

# Enable this for finding optimization-dependent bugs
# QMAKE_CFLAGS_RELEASE += -g

DEFINES += \
    _GNU_SOURCE \
    _POSIX_C_SOURCE=200112L

SOURCES += \
    adopt.c \
    arguments.c \
    comm.c \
    coop-chown.c \
    offer.c \
    privileges.c

HEADERS += \
    adopt.h \
    arguments.h \
    comm.h \
    logging.h \
    offer.h \
    privileges.h
