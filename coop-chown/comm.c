#include <errno.h>
#include <stdio.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>

#include "./arguments.h"
#include "./comm.h"
#include "./logging.h"

#define ARRAY_SIZE( A ) ( sizeof( A ) / sizeof( ( A )[ 0 ] ) )

static const char* SOCKET_PATTERN = "/tmp/.coop-chown.%u.%u.%lu";

static void convert_address( const comm_address* address_in, struct sockaddr_un* address_out )
{
    size_t path_max_size = ARRAY_SIZE( address_out->sun_path );
    address_out->sun_family = AF_UNIX;
    int32_t res = snprintf( address_out->sun_path
                          , path_max_size
                          , SOCKET_PATTERN
                          , address_in->offerer
                          , address_in->requester
                          , address_in->target_file );
    if( res < 0 || res >= (int64_t) path_max_size )
    {
        printf_die( "Path too long, maximum is %lu characters\n", path_max_size );
    }
}

static void erase_sock_file( const struct sockaddr_un* address )
{
    printf_vv( g_args.verbose, "Trying to unlink %s\n", address->sun_path );
    if( unlink( address->sun_path ) == -1 )
    {
        printf_vv( g_args.verbose, "Ignoring unlink() failure on %s\n", address->sun_path );
    }
}

static void close_requester( comm_server* server )
{
    if( server->requester_fd >= 0 )
    {
        printf_vv( g_args.verbose
                 , "Attempting to close() requester socket %d\n"
                 , server->requester_fd );
        if( close( server->requester_fd ) == -1 )
        {
            printf_die( "Could not close() requester socket %d\n", server->requester_fd );
        }
        server->requester_fd = -1;
    }
}

static void send_all( int32_t fd, const char* buf, size_t len )
{
    const char* buf_end = buf + len;
    while( true )
    {
        errno = 0;
        ssize_t res = send( fd, buf, buf_end - buf, 0 );
        if( res == -1 )
        {
            if( errno != EINTR )
            {
                printf_die( "send() failed on file descriptor %u, "
                            "%ld bytes sent successfully\n"
                          , fd
                          , res );
            }
        }
        else
        {
            buf += res;
            if( buf >= buf_end )
            {
                break;
            }
        }
    }
}

static void recv_all( int32_t fd, char* buf, size_t len )
{
    char* buf_end = buf + len;
    while( true )
    {
        errno = 0;
        ssize_t res = recv( fd, buf, buf_end - buf, MSG_WAITALL );
        if( res == -1 )
        {
            if( errno != EINTR )
            {
                printf_die( "recv() failed on file descriptor %u, "
                            "%ld bytes received successfully\n"
                          , fd
                          , res );
            }
        }
        else
        {
            buf += res;
            if( buf >= buf_end )
            {
                break;
            }
        }
    }
}

static bool accept_socket( comm_server* server )
{
    printf_vv( g_args.verbose
             , "Waiting for a connection on server socket %d\n"
             , server->socket_fd );
    errno = 0;
    server->requester_fd = accept( server->socket_fd, NULL, NULL );
    if( server->requester_fd == -1 )
    {
        switch( errno )
        {
        case EBADF:
        case EINVAL:
        case EMFILE:
        case ENFILE:
        case ENOBUFS:
        case ENOMEM:
        case ENOTSOCK:
        case EOPNOTSUPP:
        case EPERM:
            comm_delete_server( server );
            printf_die( "accept() failed irrecoverably\n" );
        default:
            printf_vv( g_args.verbose, "accept() failed\n" );
            break;
        }
        return false;
    }

    printf_vv( g_args.verbose
             , "Got a connection %d on server socket %d\n"
             , server->requester_fd
             , server->socket_fd );
    return true;
}

void comm_create_server( uid_t requester, ino_t target, comm_server* server )
{
    server->socket_fd           = -1;
    server->requester_fd        = -1;
    server->address.offerer     = getuid();
    server->address.requester   = requester;
    server->address.target_file = target;

    printf_vv( g_args.verbose, "Opening a server socket\n" );
    server->socket_fd = socket( AF_UNIX, SOCK_STREAM, 0 );
    if( server->socket_fd == -1 )
    {
        printf_die( "Could not create server socket\n" );
    }

    printf_vv( g_args.verbose, "Binding server socket %d\n", server->socket_fd );
    struct sockaddr_un address;
    convert_address( &server->address, &address );

    erase_sock_file( &address );
    if( bind( server->socket_fd, (struct sockaddr*) &address, sizeof( address ) ) == -1 )
    {
        comm_delete_server( server );
        printf_die( "Can not bind() to server socket %d\n", server->socket_fd );
    }

    printf_vv( g_args.verbose
             , "Changing permissions of %s to rw-rw-rw-\n"
             , address.sun_path );
    if( chmod( address.sun_path, 0666 ) == -1 )
    {
        comm_delete_server( server );
        printf_die( "chmod() on %s failed\n", address.sun_path );
    }

    printf_vv( g_args.verbose, "Listening to the server socket %d\n", server->socket_fd );
    if( listen( server->socket_fd, 3 ) == -1 )
    {
        comm_delete_server( server );
        printf_die( "Can not listen() to server socket %d\n", server->socket_fd );
    }
}

void comm_wait_requester( comm_server* server )
{
    printf_v( g_args.verbose, "Waiting for a adopting request...\n" );
    while( true )
    {
        if( !accept_socket( server ) )
        {
            continue;
        }

        printf_vv( g_args.verbose
                 , "Checking connection peer ID on connection %d\n"
                 , server->requester_fd );
        struct ucred peer_creds;
        socklen_t len = sizeof( struct ucred );
        if( getsockopt( server->requester_fd, SOL_SOCKET, SO_PEERCRED, &peer_creds, &len ) == -1 )
        {
            printf_vv( g_args.verbose
                     , "getsockopt() failed on connection %d\n"
                     , server->requester_fd );
            close_requester( server );
            continue;
        }

        printf_vv( g_args.verbose, "Got adopt request from UID %u\n", peer_creds.uid );
        if( peer_creds.uid == server->address.requester )
        {
            break;
        }
        close_requester( server );
    }

    printf_v( g_args.verbose, "Got adopt request from requester. Verifying...\n" );

    printf_vv( g_args.verbose
             , "Receiving inode number from connection %d\n"
             , server->requester_fd );
    ino_t request_ino;
    recv_all( server->requester_fd, (char*) &request_ino, sizeof( request_ino ) );

    printf_vv( g_args.verbose
             , "Got inode number %lu from connection %d\n"
             , request_ino
             , server->requester_fd );
    if( request_ino != server->address.target_file )
    {
        printf_die( "Verification failed! Adopter wants different file\n" );
    }

    printf_vv( g_args.verbose
             , "Receiving set_group setting from connection %d\n"
             , server->requester_fd );
    bool set_group;
    recv_all( server->requester_fd, (char*) &set_group, sizeof( set_group ) );

    printf_vv( g_args.verbose
             , "Got set_group setting %d from connection %d\n"
             , set_group
             , server->requester_fd );
    if( set_group != g_args.set_group )
    {
        if( set_group )
        {
            printf_die( "Verification failed! Adopter wants group change\n" );
        }
        else
        {
            printf_die( "Verification failed! Adopter does not want group change\n" );
        }
    }
}

void comm_delete_server( comm_server* server )
{
    close_requester( server );

    if( server->socket_fd >= 0 )
    {
        printf_vv( g_args.verbose, "Trying to close server socket %d\n", server->socket_fd );
        if( close( server->socket_fd ) == -1 )
        {
            printf_die( "Could not close() server socket %d\n", server->socket_fd );
        }
    }

    struct sockaddr_un address;
    convert_address( &server->address, &address );
    erase_sock_file( &address );
}

void comm_request_adopt( uid_t offerer, ino_t target )
{
    printf_vv( g_args.verbose, "Opening a adopt socket\n" );
    int32_t socket_fd = socket( PF_LOCAL, SOCK_STREAM, 0 );
    if( socket_fd == -1 )
    {
        printf_die( "Could not create adopt socket\n" );
    }

    printf_vv( g_args.verbose, "Connecting adopt socket %d\n", socket_fd );
    comm_address address_comm;
    address_comm.offerer     = offerer;
    address_comm.requester   = getuid();
    address_comm.target_file = target;
    struct sockaddr_un address;
    convert_address( &address_comm, &address );
    if( connect( socket_fd, (struct sockaddr_un*) &address, sizeof( address ) ) == -1 )
    {
        printf_die( "Can not connect() to adopt socket %d\n", socket_fd );
    }

    printf_vv( g_args.verbose, "Sending inode number %lu to adopt socket %d\n", target, socket_fd );
    send_all( socket_fd, (char*) &target, sizeof( target ) );

    printf_vv( g_args.verbose
             , "Sending set_group setting %d to adopt socket %d\n"
             , g_args.set_group
             , socket_fd );
    send_all( socket_fd, (char*) &g_args.set_group, sizeof( g_args.set_group ) );

    printf_vv( g_args.verbose, "Closing adopt socket %d\n", socket_fd );
    if( close( socket_fd ) == -1 )
    {
        printf_die( "Can not close() adopt socket %d\n", socket_fd );
    }
}
