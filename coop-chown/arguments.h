#ifndef ARGUMENTS_H
#define ARGUMENTS_H

#include <stdbool.h>
#include <stdint.h>

#include <unistd.h>

typedef struct chown_args
{
    bool         has_offer;
    bool         set_group;
    uid_t        offer_to;
    uint_fast8_t verbose;
    const char*  path;
} chown_args;

extern chown_args g_args;

// Initializes the global argument structure
void args_init( void );

// Parses command line arguments
void args_parse( int32_t argc, char** argv );

#endif // ARGUMENTS_H
